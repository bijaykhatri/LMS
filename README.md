Java Fx based Project using MVC architecture for fulfillment of Modern programming practices

Software development analysis was performed there by gathering what would be the classes for the applicatoin. Class diagram was then drawn. It was 
a collaborative effort then we figure out the object communication within the boundry and moved to the desgin phases we ended up desgining the 
sequence diagram.

For real time synchronization and versioning of the application repo was created at gitlab and we use IntelliJ for Integrated Develeopment Environment
to create/manage the application.

The project Members for the project are
1. Bijay Khatri - 985060
2. Sandip Tripathi - 985076
3. Prajay Mulmi - 984950
4. Tara Prasad Adhikari - 984885

The following use cases was coverd.
1. Login to the System with different authorization
2. Add Book to the Library
3. Add Book Copies to the Library
4. Add Member to the Library
5. Edit Member 
6. Checkout the Book
7. Manage Checkout Record
8. View Dashboard

Then use cases was divided among the users as follow
Bijay Khatri
 --> 1. Edit Member Copies to the Library (Use case No. 3)
 --> 2. Worked for portion of checkout record management (Use case No. 7)
 --> 3. Manage Dashboard (Use Case No. 8)
 
 Sandip Tripathi
    --> 1. Checkout the Book ( Use case No. 6)
    --> 2. Manage Checkout Record ( Use case No. 7)
    
Prajay Mulmi
    --> 1. Add Book to the Library (Use case No. 4)
    --> 2. Worked on Portion of checkout book (Use case no.6)
    
Tara Prasad Adhikari
    --> 1. Login to the System (Use case No.: 1)
    --> 2. Add Member to the Library (Use Case No.:2)
    

    