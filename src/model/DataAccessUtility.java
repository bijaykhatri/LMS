package model;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DataAccessUtility {

    /**
     * Save the serialized objects to the file system
     * @param dir the output directory
     * @param file the name of the file
     * @param object the object to be saved
     */

    public static void saveObject(String dir, String file, Object object) {
        ObjectOutputStream out = null;
        try {
            Path path = FileSystems.getDefault().getPath(dir, file);
            out = new ObjectOutputStream(Files.newOutputStream(path));
            out.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();//it should be handled in different way (return error) message, or for this project it is ok to do this action
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public static <T> List<T> readAllObject(String dir) {
        File folder = new File(dir);
        String[] fileNames = folder.list();
        List<T> list = new ArrayList<T>();
        for (int i = 0; i < fileNames.length; i++) {
            T t = (T) readObject(dir, fileNames[i]);
            list.add(t);
        }
        return list;
    }

    private static Object readObject(String dir, String file) {
        ObjectInputStream in = null;
        Object entity = null;
        try {
            Path path = FileSystems.getDefault().getPath(dir, file);
            in = new ObjectInputStream(Files.newInputStream(path));
            entity = in.readObject();

        }

            catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
        }
        return entity;
    }

    public static <T> T readGenericObject(String dir, String keyword) {
        ObjectInputStream in = null;
        T entity = null;
        try {
            Path path = FileSystems.getDefault().getPath(dir, keyword);
            in = new ObjectInputStream(Files.newInputStream(path));
            entity = (T) in.readObject();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
        }
        return entity;
    }
}
