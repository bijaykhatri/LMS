package model.POJO;

import java.io.Serializable;

public class BookCopy implements Serializable {

    private int uniqueId;
    public boolean isAvailable() {
        return isAvailable;
    }


    private boolean isAvailable;
    private Book book;

    public BookCopy(){

    }
    public BookCopy(boolean available, int uniqueId, Book book) {
        this.isAvailable = available;
        this.uniqueId = uniqueId;
        this.book = book;
    }

    public void setAvailable(boolean isAvailable){
      this.isAvailable = isAvailable;
    }
    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getCopynumber() {
        return this.uniqueId;
    }

    public void setCopynumber(int uniqueId) {
        this.uniqueId = uniqueId;
    }



    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

}
