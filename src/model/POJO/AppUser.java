package model.POJO;

import java.io.Serializable;

/**
 * Java Class responsible for managine Application level users
 * Created by Bijay on 3/2/2016.
 */
public class AppUser implements Serializable {

    public enum AuthorizationLevel {ADMINISTRATOR, LIBRARIAN, BOTH};

    private String username;
    private String password;
    private boolean active = true;
    private AuthorizationLevel role;

    public AuthorizationLevel getRole() {
        return role;
    }

    public void setRole(AuthorizationLevel role) {
        this.role = role;
    }


    public AppUser(String username, String password, boolean active) {
        this.username = username;
        this.password = password;
        this.active = active;
    }

    public AppUser(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
