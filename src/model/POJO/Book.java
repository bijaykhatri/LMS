package model.POJO;

import java.io.Serializable;
import java.util.List;

public class Book implements Serializable {
    
    private String title;
    private String isbn;

    public int getLendableDays() {
        return lendableDays;
    }

    public void setLendableDays(int lendableDays) {
        this.lendableDays = lendableDays;
    }

    private int lendableDays;
    private List<Author> authors;
    private List<BookCopy> copies;


    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return this.isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<BookCopy> getCopies() {
        return copies;
    }

    public void setCopies(List<BookCopy> copies) {
        this.copies = copies;
    }
}
