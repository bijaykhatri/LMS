package model.POJO;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class CheckoutEntry implements Serializable {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;//same as database id
    private LocalDate checkoutDate;
    private LocalDate dueDate;

    public CheckoutEntry(int id, LocalDate checkoutDate, LocalDate dueDate) {
        this.id = id;
        this.checkoutDate = checkoutDate;
        this.dueDate = dueDate;
    }
    public CheckoutEntry(int id, LocalDate checkoutDate, LocalDate dueDate,BookCopy bookCopy) {
        this.id = id;
        this.checkoutDate = checkoutDate;
        this.dueDate = dueDate;
        this.bookCopy =bookCopy;
    }

    private BookCopy bookCopy;

    
    public LocalDate getCheckoutDate() {
        return this.checkoutDate;
    }

    public void setCheckoutDate(LocalDate checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public LocalDate getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public BookCopy getBookCopy() {
        return bookCopy;
    }

    public void setBookCopy(BookCopy bookCopy) {
        this.bookCopy = bookCopy;
    }
    


}
