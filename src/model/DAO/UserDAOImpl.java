/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import model.DataAccessUtility;
import model.POJO.LibraryMember;

import java.util.List;
import java.util.StringJoiner;


public class UserDAOImpl implements UserDAO {
    private static final String OUTPUT_DIR =System.getProperty("user.dir")+"//resources//data//users";

    @Override
    public boolean addMember(LibraryMember libraryMember){
        String keyword = String.valueOf(libraryMember.getId());
        DataAccessUtility.saveObject(OUTPUT_DIR, keyword, libraryMember);
        return true;
    }

    @Override
    public LibraryMember findMember(String fieldName) {
        return DataAccessUtility.readGenericObject(OUTPUT_DIR, fieldName);
    }

    @Override
    public List<LibraryMember> getAll() {
        return DataAccessUtility.readAllObject(OUTPUT_DIR);
    }


}
