package model.DAO;

import model.DataAccessUtility;
import model.POJO.Book;
import model.POJO.BookCopy;

import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public class BookCopyDAOImpl implements BookCopyDAO{

    private static final String OUTPUT_DIR =System.getProperty("user.dir")+"\\resources\\data\\bookcopies";


    @Override
    public int getCopy(Book book) {
        //BookCopy bookCopy= (DataAccessUtility.readGenericObject(OUTPUT_DIR, book.getIsbn()));
       // if(bookCopy == null) return 0;
        //return bookCopy.getCopynumber();
        return 0;
    }

    @Override
    public boolean checkAvailability(Book book) {
        return getCopy(book) > 0;
    }

    @Override
    public boolean save(BookCopy bookCopy) {
        DataAccessUtility.saveObject(OUTPUT_DIR,String.valueOf(bookCopy.getBook().getIsbn()),bookCopy);
        return true;
    }
}
