package model.DAO;

import model.POJO.CheckoutRecord;

/**
 * Created by Bijay on 3/2/2016.
 */
public interface CheckoutRecordDAO {

    void editCheckoutRecord(CheckoutRecord checkoutRecord);
}
