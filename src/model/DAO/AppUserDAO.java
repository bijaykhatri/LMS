package model.DAO;

import model.POJO.AppUser;

import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public interface AppUserDAO {

    AppUser getUser(String username);

    void setUser(AppUser user);

    boolean addUser(AppUser user);

    List<AppUser> getAll();

    boolean isValid(AppUser user);
}
