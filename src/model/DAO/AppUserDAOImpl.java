package model.DAO;

import model.DataAccessUtility;
import model.POJO.AppUser;

import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public class AppUserDAOImpl implements AppUserDAO {

    AppUser user;
    private static final String OUTPUT_DIR =System.getProperty("user.dir")+"//resources//data//appusers";

    @Override
    public AppUser getUser(String username) {

        return DataAccessUtility.readGenericObject(OUTPUT_DIR, username);
    }

    @Override
    public boolean addUser(AppUser user) {
        DataAccessUtility.saveObject(OUTPUT_DIR, user.getUsername(), user);
        return true;
    }

    @Override
    public void setUser(AppUser user){

        this.user = user;
    }

    @Override
    public List<AppUser> getAll() {

        return DataAccessUtility.readAllObject(OUTPUT_DIR);
    }

    @Override
    public boolean isValid(AppUser user) {

        //Check the existence of the users in data store
        //AppUserDAOImpl appUserDAO = new AppUserDAOImpl();

        List<AppUser> usersList = this.getAll();
        boolean isNull = usersList.isEmpty();
        boolean valid = false;
        if (isNull) {
            return false;
        }
        else {
            for (AppUser appUser : usersList ) {
                if (appUser != null) {
                    if(appUser.getUsername().equals(user.getUsername()) && appUser.getPassword().equals(user.getPassword())){
                      valid = true;
                        break;
                    }

                }

            }
        }
        return valid;

    }
}
