package model.DAO;

import model.DataAccessUtility;
import model.POJO.Book;
import model.POJO.BookCopy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public class BookDAOImpl implements BookDAO {

    private static final String OUTPUT_DIR =System.getProperty("user.dir")+"//resources//data//books";


    @Override
    public boolean addBook(Book book){

        DataAccessUtility.saveObject(OUTPUT_DIR, book.getIsbn(), book);
        return true
                ;
    }

    /**
     * get All books out of data store
     * @return List of available books
     */
    @Override
    public List<Book> getAll(){
        return DataAccessUtility.readAllObject(OUTPUT_DIR);
    }

    /**
     * get Book out of datastore by ISBN
     * @param isbn
     * @return Book
     */
    @Override
    public Book getBook(String isbn){
        return DataAccessUtility.readGenericObject(OUTPUT_DIR, isbn);
    }

    @Override
    public BookCopy findBookForCheckout(String isbn) {
        Book book = getBook(isbn);
        for (BookCopy bookCopy : book.getCopies()) {
            if (bookCopy.isAvailable()){
                return  bookCopy;
            }
        }
        return  null;
    }


}
