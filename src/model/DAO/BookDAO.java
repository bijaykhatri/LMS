package model.DAO;

import model.POJO.Book;
import model.POJO.BookCopy;

import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public interface BookDAO {

    boolean addBook(Book book);

    List<Book> getAll();

    Book getBook(String isbn);

    public BookCopy findBookForCheckout(String isbn);



}
