package model.DAO;

import model.POJO.Book;
import model.POJO.BookCopy;

/**
 * Created by Bijay on 3/2/2016.
 */
public interface BookCopyDAO {

    int getCopy(Book book);

    boolean checkAvailability(Book book);

    public boolean save(BookCopy bookCopy);
}
