package model.DAO;

import model.POJO.LibraryMember;

import java.util.List;

/**
 * Created by Bijay on 3/2/2016.
 */
public interface UserDAO {
    boolean addMember(LibraryMember libraryMember);
    LibraryMember findMember(String fieldName);
    List<LibraryMember> getAll();
}
