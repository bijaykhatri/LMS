/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import helper.Helper;
import javafx.scene.image.Image;
import javafx.stage.StageStyle;
import model.DAO.AppUserDAO;
import model.DAO.AppUserDAOImpl;
import java.net.URL;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.Alert;
import model.POJO.AppUser;
import helper.Validator;


public class LoginController implements Initializable {

    @FXML
    private TextField userNameField;
    @FXML
    private TextField passwordField;

    @FXML
    private Button signInButton;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    


    public void handleOnEnter(ActionEvent event) {
            handleSignInButtonAction(event);
        }
    
    @FXML
    public void handleSignInButtonAction(ActionEvent event) {
        Stage mainStage = new Stage();
        Stage stage = (Stage) signInButton.getScene().getWindow();
        
        Helper.log("Sign In application loaded");

        //Create a user class to verify if username and password 
        //informed at login are equal to DAO user credentials
        AppUser user = new AppUser();
        String userName ="";
        String password ="";

        userName = userNameField.getText().toLowerCase();
        password = passwordField.getText().toLowerCase();

        if(Validator.isEmpty(userName) || Validator.isEmpty(password)){
            showAlert("Please enter username and password first");
            return;
        }

        user.setUsername(userName);
        user.setPassword(password);
        
        AppUserDAO userDAO = new AppUserDAOImpl();
        
        if (userDAO.isValid(user) == false) {
            showAlert("Username or Password invalid!", Alert.AlertType.WARNING);

        }
        else {
            userDAO.setUser(user);
            user.setRole(userDAO.getUser(user.getUsername()).getRole());
            
            try {
                Helper.log("Attempting to Start Main Frame...");
                Helper.log("Setting FXML file");
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FormMain.fxml"));
                MainController controller = new MainController(user);
                loader.setController(controller);
                Parent root = (Parent) loader.load();
                controller.setUser(user);

                Helper.log("Loading FXML scene for Main Frame");
                Helper.log("Setting scene");
                Scene scene = new Scene(root);
                mainStage.getIcons().add(new Image("/images/title-icon.jpg"));
                mainStage.setScene(scene);
                mainStage.setResizable(false);

                //closing previous screen (login screen)
                Helper.log("Closing previous form");
                stage.close();
                mainStage.show();
            }
            catch(Exception e) {
                e.printStackTrace();

            }

        }
            
    }

    private void showAlert(String message, Alert.AlertType type){
        Alert alert = new Alert(type);
        alert.setTitle("Library Management System");
        alert.setHeaderText("Got you...!!!");
        alert.setContentText(message);
        alert.showAndWait();
        userNameField.requestFocus();

    }
    private void showAlert(String message){
        showAlert(message, Alert.AlertType.INFORMATION);

    }
}
