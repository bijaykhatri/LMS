package controller;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

import helper.Helper;
import helper.Validator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DAO.AppUserDAOImpl;
import model.DAO.UserDAO;
import model.DAO.UserDAOImpl;
import model.POJO.CheckoutRecord;
import model.POJO.LibraryMember;


public class MemberController extends BaseController {

    @FXML
    private ComboBox state;
    
    @FXML
    private ComboBox city;
    
    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;
    
    @FXML
    private TextField phoneNumber;
    
    @FXML
    private Button add;


    @FXML
    private TextField street;
    
    @FXML
    private TextField zip;

    @FXML
    private ComboBox memberList;

    private UserDAO userDAO;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        state.getItems().clear();

        state.getItems().addAll(
                "Iowa",
                "Chicago",
                "California",
                "Texas");
        
        city.getItems().clear();

        city.getItems().addAll(
                "fairfield",
                "New York",
                "Jersy",
                "Boston",
                "Dakota");

        //listener for Address
        add.setOnAction(event -> addMember());

        city.setValue(city.getItems().get(0));
        state.setValue(state.getItems().get(0));


    }
    
    public void addMember(){
        if(checkForm()) {
            Helper.log("Starting to add member: ");
            userDAO = new UserDAOImpl();
            LibraryMember newUser = new LibraryMember();
            System.out.println("First Name of Member: " + firstName.getText());
            newUser.setFirstName(firstName.getText());
            newUser.setLastName(lastName.getText());
            newUser.setAddress(String.format("%s,%s,%s,%s",
                    street.getText(), city.getValue().toString(), state.getValue().toString(), zip.getText()));
            newUser.setPhoneNumber(phoneNumber.getText());
            int id = new Random().nextInt();
            System.out.println("Member id Created= " + id);
            newUser.setId(id < 0 ? id * -1 : id);
            newUser.setCheckoutRecord(new CheckoutRecord());
            boolean res = userDAO.addMember(newUser);
            if (res) {
                Helper.showAlert("Member addition successful");
            } else {
                Helper.showAlert("Sorry! Member not added", Alert.AlertType.ERROR);
            }
            Stage thisStage = (Stage) add.getScene().getWindow();
            thisStage.close();
        }
        else
        {
            Helper.showAlert("Sorry! All fields required. Zip code shoud be numeric only");
        }

    }

    private boolean checkForm() {
        if(Validator.isEmpty(firstName.getText()))
            return false;
        if(Validator.isEmpty(lastName.getText()))
            return false;
        if(Validator.isEmpty(phoneNumber.getText()))
            return false;
        if(Validator.isEmpty(zip.getText()))
            return false;
        if(Validator.isEmpty(zip.getText()) || !Validator.isNumericOnly(zip.getText()))
            return false;
        return true;
    }


}
