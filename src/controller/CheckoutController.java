package controller;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.time.LocalDate;
import java.util.Random;
import java.util.ResourceBundle;
import helper.Helper;
import helper.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.DAO.BookDAOImpl;
import model.DAO.UserDAOImpl;
import model.POJO.*;

public class CheckoutController extends BaseController {

    @FXML
    private Button exportButton;

    @FXML
    private TextField searchTextMember;
    @FXML
    private Text firstName;

    @FXML
    private Text lastName;

    @FXML
    private Text bookName;

    @FXML
    private TextField searchTextBook;

    @FXML
    private Label messageLabel;

    @FXML
    private DatePicker dueDatePicker;



    private LibraryMember libraryMember;
    private Book book;
    private BookCopy bookCopy;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    public void handleExportButtonAction(ActionEvent event) {
        
        Stage stage = new Stage();
        
        Helper.log("Exporting data to file .csv...");
        
        String defaultFileName = "LibrarySystemExport.csv";
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save file");
        fileChooser.setInitialFileName(defaultFileName);
        File savedFile = fileChooser.showSaveDialog(stage);
        if (savedFile != null) {
            try {
                saveFileRoutine(savedFile);
            }
            catch(Exception e) {
                e.printStackTrace();
                //actionStatus.setText("An ERROR occurred while saving the file!");
                return;
            }
            //actionStatus.setText("File saved: " + savedFile.toString());
        }
        else {
            //actionStatus.setText("File save cancelled.");
        }

        
        Helper.log("Export complete");
    }
    
    private void saveFileRoutine(File file)
			throws Exception{
		
        try {
            // Creates a new file and writes the txtArea contents into it
            String txt = "Text content";

            file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(txt);
            writer.close();
        } catch(Exception e) {
                            e.printStackTrace();                            
            }
	}

    public void searchMember(){
        clear();
        String memberId = searchTextMember.getText();
        if(!Validator.isNumericOnly(memberId) || memberId.length() == 0){
            Helper.showAlert("Member Id should be numeric of length greater than 0");
            return;
        }
        UserDAOImpl userDAO = new UserDAOImpl();
        libraryMember = userDAO.findMember(memberId);
        if (libraryMember!=null){
            String  libraryMemberFirstName = libraryMember.getFirstName();
            String libraryMemberLastName = libraryMember.getLastName();
            firstName. setText("Member : "+libraryMemberFirstName+" "+libraryMemberLastName);

        }else {
            Helper.showAlert("User does not exits");

        }
    }
    String bookIsbn;
    public void searchBook(){
        bookIsbn = searchTextBook.getText();
        if(!Validator.isNumericOnly(bookIsbn) || bookIsbn.length() !=13){
            Helper.showAlert("Book ISBN should be numeric and of 13 digit");
            return;
        }
        if (libraryMember!=null){  // library member will be set only when valid member is found by search Member

           // if (!isNotReturned){
                BookDAOImpl bookDAO = new BookDAOImpl();
                 bookCopy = bookDAO.findBookForCheckout(bookIsbn);
                if (bookCopy!=null){
                    messageLabel.setStyle(
                            "-fx-background-color: #f4f4f4;"+
                            "-fx-text-fill: #6ad662;"+
                            "-fx-max-width: 80px;"
                    );
                    messageLabel.setText("Available");
                    String  name = bookCopy.getBook().getTitle();
                    bookName.setText("Book : "+name);
                }else {
                    messageLabel.setStyle(
                            "-fx-background-color: #ff3c51;"+
                            "-fx-text-fill: #fcfcfc;"+
                            "-fx-max-width: 100px;"
                    );
                  messageLabel.setText("Not Available");
                }
           // }

        }
        else{
            Helper.showAlert("Book does not exits");
        }
    }

    public void submitCheckout(){
        Helper.log("Submitting checkout");
        LocalDate dueDate = dueDatePicker.getValue();
        if (libraryMember !=null &&  bookCopy != null){
            int uniqueId = new Random().nextInt();
            CheckoutEntry checkoutEntry = new CheckoutEntry(uniqueId<0?uniqueId*-1:uniqueId, LocalDate.now(),
                     dueDate, bookCopy);
            CheckoutRecord checkoutRecord = libraryMember.getCheckoutRecord();
            checkoutRecord.getCheckoutEntries().add(checkoutEntry);
            new UserDAOImpl().addMember(libraryMember);
            //so it is saved successfully
            Book book = bookCopy.getBook();
            for(BookCopy bCopy : book.getCopies()){
                if(bCopy.getUniqueId() == bookCopy.getUniqueId())
                    bCopy.setAvailable(false);
            }
            new BookDAOImpl().addBook(book);

            //("Checkout record saved for "+libraryMember.getFirstName()+ " "+libraryMember.getLastName());
            ((Stage) firstName.getScene().getWindow()).close();
        }

    }

    /**
     * clears out the library member, book and corresponding label
     */

    public void clear(){
        libraryMember= null;
        book=null;
        messageLabel.setText("");
        searchTextBook.setText("");
    }
}
