package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.stage.Stage;
import helper.Helper;


public abstract class BaseController implements Initializable {

    @FXML
    Button cancelButton; //any saving form contain cancel button


    BaseController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    public void handleCancelButtonAction(ActionEvent event) {
        Stage stage = (Stage) cancelButton.getScene().getWindow();

        Helper.log("Cancelling action/form");

        stage.close();
    }



}
