package controller;

import helper.Helper;
import helper.Validator;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DAO.UserDAO;
import model.DAO.UserDAOImpl;
import model.POJO.CheckoutRecord;
import model.POJO.LibraryMember;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;


public class EditMemberController extends BaseController {

    @FXML
    private ComboBox state;
    
    @FXML
    private ComboBox city;
    
    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;
    
    @FXML
    private TextField phoneNumber;

    @FXML
    private Button editMember;

    @FXML
    private TextField street;
    
    @FXML
    private TextField zip;

    @FXML
    private ComboBox memberList;

    private UserDAO userDAO;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        state.getItems().clear();

        memberList.getItems().clear();
        new UserDAOImpl().getAll().forEach(member -> memberList.getItems().add(
                member.getFirstName()+" "+member.getLastName()+"-"+member.getId()));

        //set first item selected
        memberList.setValue(memberList.getItems().get(0));

        //add listener to change value property of member list
        memberList.valueProperty().addListener((observable, oldValue, newValue) -> populateField(newValue.toString()));
        state.getItems().addAll(
                "Iowa",
                "Chicago",
                "California",
                "Texas");
        
        city.getItems().clear();

        city.getItems().addAll(
                "fairfield",
                "New York",
                "Jersy",
                "Boston",
                "Dakota");


        city.setValue(city.getItems().get(0));
        state.setValue(state.getItems().get(0));


    }

    public void editMember(){
        if(checkForm()) {
            Helper.log("Starting to edit member: ");
            String id = memberList.getValue().toString().split("-")[1];
            ;
            userDAO = new UserDAOImpl();
            LibraryMember editUser = userDAO.findMember(id);
            editUser.setFirstName(firstName.getText());
            editUser.setLastName(lastName.getText());
            editUser.setAddress(String.format("%s,%s,%s,%s",
                    street.getText(), city.getValue().toString(), state.getValue().toString(), zip.getText()));
            editUser.setPhoneNumber(phoneNumber.getText());
            System.out.println("Member id Created= " + id);
            //editUser.setId(id);
            editUser.setCheckoutRecord(new CheckoutRecord());
            boolean res = userDAO.addMember(editUser);
            if (res) {
                Helper.showAlert("Member edited successful");
            } else {
                Helper.showAlert("Sorry! Member not edited", Alert.AlertType.ERROR);
            }
            Stage thisStage = (Stage) editMember.getScene().getWindow();
            thisStage.close();
        }
        else
        {
            Helper.showAlert("Sorry! All fields required. Zip code shoud be numeric only");
        }

    }

    private boolean checkForm() {
        if(Validator.isEmpty(firstName.getText()))
            return false;
        if(Validator.isEmpty(lastName.getText()))
            return false;
        if(Validator.isEmpty(phoneNumber.getText()))
            return false;
        if(Validator.isEmpty(zip.getText()) || !Validator.isNumericOnly(zip.getText()))
            return false;
        return true;
    }

    public void populateField(String value){
        String id = memberList.getValue().toString();
        String editMemberId = id.split("-")[1];
        Helper.log("Editing info for Member: "+editMemberId);
        LibraryMember member = new UserDAOImpl().findMember(editMemberId);

        //set fields
        firstName.setText(member.getFirstName());
        lastName.setText(member.getLastName());
        phoneNumber.setText(member.getPhoneNumber());
        String[] address = member.getAddress().split(",");
        street.setText(address[0]);
        city.setValue(address[1]);
        state.setValue(address[2]);
        zip.setText(address[3]);

    }

}
