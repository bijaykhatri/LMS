package controller;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

import helper.Helper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import model.DAO.UserDAOImpl;
import model.POJO.*;


public class CheckoutRecordController extends BaseController {

    @FXML
    private ChoiceBox book;

    @FXML
    private TextField textMemberId;
    
    @FXML
    private TableView<Checkout> tableView;

    @FXML
    private TableColumn<Checkout,String> checkoutIdCol;
    @FXML
    private TableColumn<Checkout,String> memberCol;
    @FXML
    private TableColumn<Checkout,String> bookCol;
    @FXML
    private TableColumn<Checkout,String> checkoutDateCol;
    @FXML
    private TableColumn<Checkout,String> returnDateCol;


    
    @FXML
    private Button export;
    
    @FXML
    private Button ok;

    private ObservableList<Checkout> checkoutData = FXCollections.observableArrayList();
    private ObservableList<Checkout> empty = FXCollections.observableArrayList();
    private  LibraryMember member ;


    public void initialize(URL url, ResourceBundle rb){
        System.out.println("Initialization Called");
        checkoutIdCol.setCellValueFactory(new PropertyValueFactory<Checkout, String>("checkoutId"));
        memberCol.setCellValueFactory(new PropertyValueFactory<Checkout, String>("member"));
        bookCol.setCellValueFactory(new PropertyValueFactory<Checkout, String>("bookTitle"));
        checkoutDateCol.setCellValueFactory(new PropertyValueFactory<Checkout, String>("CheckoutDate"));
        returnDateCol.setCellValueFactory(new PropertyValueFactory<Checkout, String>("ReturnDate"));
    }

    public void searchMember(){
        String memberId = textMemberId.getText();
        UserDAOImpl userDAO = new UserDAOImpl();
        member = userDAO.findMember(memberId);
        if (member!=null){
            checkoutData.clear();
            populateCheckoutData();
        }

        tableView.setItems(checkoutData);
    }

     private  void populateCheckoutData(){
         checkoutData=empty;
         CheckoutRecord checkoutRecord = member.getCheckoutRecord();
           List<CheckoutEntry> entries = checkoutRecord.getCheckoutEntries();
         for (CheckoutEntry entry : entries) {
             String checkoutId =  String.valueOf(entry.getId());
             BookCopy bookCopy = entry.getBookCopy();
             String bookTitle = bookCopy.getBook().getTitle();
             checkoutData.add(new Checkout(checkoutId,member.getFirstName()+" "+
                     member.getLastName(),bookTitle,LocalDate.now()));
         }

     }
    public class Checkout {
        private String checkoutId;

        public String getMember() {
            return member;
        }

        public void setMember(String member) {
            this.member = member;
        }

        private String member;

        public Checkout(String checkoutId,String member, String bookTitle, LocalDate checkoutDate, LocalDate returnDate) {
            this.checkoutId = checkoutId;
            this.bookTitle = bookTitle;
            this.checkoutDate = checkoutDate;
            this.returnDate = returnDate;
            this.member = member;
        }

        public Checkout(String checkoutId,String member, String bookTitle, LocalDate checkoutDate) {
            this.checkoutId = checkoutId;
            this.bookTitle = bookTitle;
            this.checkoutDate = checkoutDate;
            this.member = member;
        }
        public String getBookTitle() {

            return bookTitle;
        }

        public void setBookTitle(String bookTitle) {

            this.bookTitle = bookTitle;
        }

        public String getCheckoutId() {

            return checkoutId;
        }

        public void setCheckoutId(String checkoutId) {

            this.checkoutId = checkoutId;
        }

        public LocalDate getCheckoutDate() {
            return checkoutDate;
        }

        public void setCheckoutDate(LocalDate checkoutDate) {
            this.checkoutDate = checkoutDate;
        }

        public LocalDate getReturnDate() {
            return returnDate;
        }

        public void setReturnDate(LocalDate returnDate) {
            this.returnDate = returnDate;
        }

        private String bookTitle;
        private LocalDate checkoutDate;
        private LocalDate returnDate;
    }
    
}
