
package controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import helper.Helper;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import model.DAO.AppUserDAOImpl;
import model.DAO.BookDAOImpl;
import model.DAO.UserDAOImpl;
import view.controller.BookApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.Optional;



import javafx.application.Platform;
import model.POJO.AppUser;

import javax.management.timer.Timer;


public class MainController implements Initializable {

    public MainController(AppUser user) {
        this.setUser(user);
    }
    
    @FXML
    public Button checkoutButton;

    @FXML
    public Button checkoutRecordButton;

    @FXML
    public Button addMemberButton;

    @FXML
    public Button editMemberButton;

    @FXML
    public Button addBookButton;

    @FXML
    public Button addBookCopyButton;

    @FXML
    public Button logoutButton;

    @FXML
    public Button exitButton;

    //get image to be use to button
    Image addIcon = new Image(getClass().getResourceAsStream("/images/plus.png"));
    Image editIcon = new Image(getClass().getResourceAsStream("/images/edit.png"));
    Image viewIcon = new Image(getClass().getResourceAsStream("/images/view.png"));
    Image infoIcon = new Image(getClass().getResourceAsStream("/images/info.png"));

    private AppUser user = new AppUser();
    
    
    public void setUser(AppUser appUser) {
        user = appUser ;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addMemberButton.setGraphic(new ImageView(addIcon));
        addMemberButton.setContentDisplay(ContentDisplay.RIGHT);


        addBookButton.setGraphic(new ImageView(addIcon));
        addBookButton.setContentDisplay(ContentDisplay.RIGHT);

        addBookCopyButton.setGraphic(new ImageView(addIcon));
        addBookCopyButton.setContentDisplay(ContentDisplay.RIGHT);

        editMemberButton.setGraphic(new ImageView(editIcon));
        editMemberButton.setContentDisplay(ContentDisplay.RIGHT);

        checkoutButton.setGraphic(new ImageView(addIcon));
        checkoutButton.setContentDisplay(ContentDisplay.RIGHT);

        checkoutRecordButton.setGraphic(new ImageView(viewIcon));
        checkoutRecordButton.setContentDisplay(ContentDisplay.RIGHT);

        bookStatusLabel.setGraphic(new ImageView(infoIcon));
        bookStatusLabel.setContentDisplay(ContentDisplay.LEFT);

        userStatusLabel.setGraphic(new ImageView(infoIcon));
        userStatusLabel.setContentDisplay(ContentDisplay.LEFT);

        disableAllButtons(true);
        
        if ((user.getRole() == AppUser.AuthorizationLevel.ADMINISTRATOR)) {
           disableButtonsForAdmin();
        }

        else if((user.getRole() == AppUser.AuthorizationLevel.LIBRARIAN)) {
            disableButtonsForLibrarian();
        }
        else{
            Helper.log("Both privilege");
            disableAllButtons(false);
        }

        //editMemberButton.setVisible(false);
        
        //for displaying current time 
        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(1000),
                ae -> getLocalTime()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        
        getBookStatus();
        getMemberStatus();

    }

    private void getMemberStatus() {
        userStatusLabel.setText("Total Available members: "+ String.valueOf(new UserDAOImpl().getAll().size()));
    }

    private void getBookStatus(){
        bookStatusLabel.setText("Total Available books: "+String.valueOf(new BookDAOImpl().getAll().size()));
    }
    @FXML
    private Label timeLabel;

    @FXML
    private Label bookStatusLabel;

    @FXML
    private Label userStatusLabel;

    private void getLocalTime() {
        timeLabel.setDisable(false);
        String time = LocalTime.now().toString();
        String date = LocalDate.now().toString();
        timeLabel.setText(date+" "+time.substring(0,8));

    }

    private void disableAllButtons(boolean signal) {
        checkoutButton.setDisable(signal);
        checkoutRecordButton.setDisable(signal);
        addMemberButton.setDisable(signal);
        editMemberButton.setDisable(signal);
        addBookButton.setDisable(signal);
        addBookCopyButton.setDisable(signal);
        logoutButton.setDisable(false);

    }
    private void disableButtonsForAdmin(){
        Helper.log("Administrator privilege");
        addMemberButton.setDisable(false);
        editMemberButton.setDisable(false);
        addBookButton.setDisable(false);
        addBookCopyButton.setDisable(false);

    }

    private void disableButtonsForLibrarian(){
        Helper.log("Librarian privileges");
        checkoutButton.setDisable(false);
        checkoutRecordButton.setDisable(false);
    }

    @FXML
    public void handleLogoutButtonAction(ActionEvent event) {
        BookApp bookApp = new BookApp();
        Stage mainStage = new Stage();
        Stage stage = (Stage) logoutButton.getScene().getWindow();
        Helper.log("Logging out user from application");
        bookApp.start(mainStage);
        stage.close();
        
    }

    @FXML
    public void handleExitButtonAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Library Management System");
        alert.setHeaderText("Closing Library System");
        alert.setContentText("Are you sure you want close the system ?");
        Optional<ButtonType> response = alert.showAndWait();
        if (response.get() == ButtonType.OK) {
            Helper.log("Closing application");
            Helper.log("Finishing application");
            Platform.exit();
        }
        
        
    }

    private void launchForm(String formURL) {
         Stage stage = new Stage();
         try {
            Helper.log("Starting " + formURL + "...");
            Helper.log("Setting FXML file");
            Parent root = FXMLLoader.load(getClass().getResource(formURL));
            Helper.log("Loading scene");
            Scene scene = new Scene(root);
            Helper.log("Setting scene stage");
            stage.setScene(scene);
            String title = formURL.substring(formURL.lastIndexOf("/")+1);
            stage.setTitle(title.substring(4,title.lastIndexOf(".")));
            stage.getIcons().add(new Image("/images/title-icon.jpg"));
            stage.setResizable(false);
            Helper.log("Showing " + formURL +  " form");
            stage.show();
            
        } catch(Exception e) {
             Helper.log(e.toString());
            e.printStackTrace();
	}      
        
    }
    
    @FXML
    public void handleCheckoutButtonAction(ActionEvent event) {

        launchForm("/fxml/FormCheckout.fxml");
    }
    @FXML
    public void handleCheckoutRecordButtonAction(ActionEvent event) {

        launchForm("/fxml/FormCheckoutRecord.fxml");
    }
    @FXML
    public void handleAddMemberButtonAction(ActionEvent event) {

        launchForm("/fxml/FormAddMember.fxml");
    }
    @FXML
    public void handleEditMemberButtonAction(ActionEvent event) {

        launchForm("/fxml/FormEditMember.fxml");
    }
    @FXML
    public void handleAddBookButtonAction(ActionEvent event) {

        launchForm("/fxml/FormAddBook.fxml");
    }
    @FXML
    public void handleAddBookCopyButtonAction(ActionEvent event) {

        launchForm("/fxml/FormAddBookCopy.fxml");
    }
    
}
