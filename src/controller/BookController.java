/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.*;

import helper.Helper;
import helper.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.DAO.BookDAO;
import model.DAO.BookDAOImpl;
import model.POJO.Author;
import model.POJO.Book;
import model.POJO.BookCopy;


/**
 * FXML Controller class
 *
 * @author Huthayfa
 */
public class BookController extends BaseController {

    @FXML
    private TextField txtISBN;
    
    @FXML
    private TextField txtTitle;
    
    //TODO: Author handling remaining
    @FXML
    private ListView authorListView;
    
    @FXML
    private ComboBox<Integer> cmbCheckoutLength;   
    
    @FXML
    private Button btnSave;

     
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cmbCheckoutLength.getItems().addAll(7, 21); //possible values of the comboBox
        cmbCheckoutLength.setValue(7);
        authorListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        authorListView.getItems().addAll("Stephen King","Jane Austen","J.K Rowling","Charles Hawel","Rreze Abdullahu",
        "Mimoza Ahmeti","Lindita Arapi","Flora Brovina","Klara Buda"
        );

        //Validations
//        txtISBN.focusedProperty().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
//                if (!newValue) { //when focus lost
//                    String textValue = txtISBN.getText();
//                    if (Validator.isEmpty(textValue) || !Validator.isNumericOnly(textValue) || textValue.length() != 13) {
//                        //when it not valid, set the textField to right message and add the field to invalidFields
//                        txtISBN.setText(txtISBN.getText().replaceFirst(" Enter 13 digit number", " Enter 13 digit number") );
//                        txtISBN.setStyle(INVALID_STYLE);
//                        if (!invalidFields.contains("ISBN"))
//                            invalidFields.add("ISBN");
//                    } else {//if it is valid, remove it from the list (either it added before or not) if not added before, nothing removed
//                        invalidFields.remove("ISBN");
//                        //txtISBN.setStyle("-fx-background-color: white;");
//                        txtISBN.setStyle(VALID_STYLE);
//                    }
//                }
//            }
//        });


        txtISBN.setText("Enter 13 digit number");
        txtISBN.focusedProperty().addListener((arg0,oldValue,newValue) ->
        {
            if(oldValue.toString().contains(" Enter"))
                txtISBN.setText("");
        });
        
    } 
    
    @FXML
    private void btnSaveAction(ActionEvent event) {
        System.out.println("Adding Book ");
        if(checkForm()) {
            Book book = new Book();
            book.setIsbn(txtISBN.getText());
            int lendableDays = cmbCheckoutLength.getValue();
            book.setLendableDays(lendableDays);
            book.setTitle(txtTitle.getText());
            List<String> authors = authorListView.getSelectionModel().getSelectedItems();
            List<Author> authorList = new ArrayList<>();
            for (String author : authors) {
                Author newAuthor = new Author();
                newAuthor.setFirstName(author);
                int id = new Random().nextInt();
                newAuthor.setId((id < 0) ? id * -1 : id);
                authorList.add(newAuthor);
            }
            book.setAuthors(authorList);
            List<BookCopy> copies = new ArrayList<BookCopy>();
            int uniqueId = new Random().nextInt();
            copies.add(new BookCopy(true, uniqueId, book));
            book.setCopies(copies);
            BookDAO bookDao = new BookDAOImpl();
            Boolean saveResult = bookDao.addBook(book);
            if (saveResult) {
                Helper.showAlert("Book Added");
            } else {
                Helper.showAlert("Cannot add Book");
            }
            ((Stage) btnSave.getScene().getWindow()).close();
        }
        else{
            Helper.showAlert("Sorry! All fields required. ISBN should be 13 digit number");
        }
    }

    private boolean checkForm() {
        if(Validator.isEmpty(txtISBN.getText()) || !Validator.isNumericOnly(txtISBN.getText()) || txtISBN.getText().trim().length() !=13)
            return false;
        if(Validator.isEmpty(txtTitle.getText()))
            return false;
        return true;

    }


}
