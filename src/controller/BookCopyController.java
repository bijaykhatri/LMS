/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DAO.BookCopyDAOImpl;
import model.DAO.BookDAOImpl;
import model.POJO.Book;
import model.POJO.BookCopy;
import helper.Helper;

/**
 * FXML Controller class
 *
 * @author mauro
 */
public class BookCopyController extends BaseController {

    @FXML
    private TextField bookCopyTextBox;

    @FXML
    private Button saveButton;

    @FXML
    private ComboBox bookCombo;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        List<Book> books= new BookDAOImpl().getAll();
        books.forEach(e -> bookCombo.getItems().add(e.getIsbn()+"-"+e.getTitle()));
        if(books.size() >=1){
            bookCombo.setValue(books.get(0).getIsbn()+"-"+books.get(0).getTitle());


            //TODO: work for getting the exact number
            int copy = new BookCopyDAOImpl().getCopy(books.get(0));
            bookCopyTextBox.setText(String.valueOf(copy));
        }
    }    
    
    
//    @FXML
//    private void btnAddBookCopyAction(ActionEvent event) {
//        Helper.log("Adding book copy...");
//
//        Stage stage = (Stage) addButton.getScene().getWindow();
//
//        //Implements save copy method here
//
//        stage.close();
//
//    }

    public void handleSaveBookCopy(ActionEvent actionEvent) {
        String isbn = bookCombo.getValue().toString().split("-")[0];
        boolean res = false;
        int copy = Integer.parseInt(bookCopyTextBox.getText());
        for (int i=0;i< copy;i++){
            BookCopy bookCopy = new BookCopy();
            bookCopy.setAvailable(true);
            bookCopy.setCopynumber(copy);
            bookCopy.setUniqueId(this.hashCode()+ new Random().nextInt(1000));
            Book book = new BookDAOImpl().getBook(isbn);
            book.getCopies().add(bookCopy);
            bookCopy.setBook((new BookDAOImpl().getBook(isbn)));
            res=  new BookDAOImpl().addBook(book);
           // boolean res = (new BookCopyDAOImpl()).save(bookCopy);
        }

       // Helper.showAlert(String.valueOf(new BookDAOImpl().getBook(isbn).getCopies().size())+"  copies");

        if (res) {
            Helper.showAlert("Copy of Book Added");
        }
        else{
            Helper.showAlert("Cannot add Book Copies");
        }
        ((Stage)saveButton.getScene().getWindow()).close();
    }

    public void handleCancel(ActionEvent actionEvent) {
        ((Stage)saveButton.getScene().getWindow()).close();
    }
}
