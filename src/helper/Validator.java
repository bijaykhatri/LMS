package helper;

public class Validator {

    public static boolean isEmpty(String str) {

        return (str == null) || (str.trim().equals(""));
    }
    
    public static boolean isNumericOnly (String str){
        
        return str.matches("\\d+");  //return true if digits only
    }
    

    public static boolean isAlphabetOnly (String str){
        
        return str.matches("[A-Za-z ]+");
    }
    
    

}
