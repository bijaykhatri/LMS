/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.control.Alert;

public class Helper {
    
    
    private static boolean writeToConsole = true;
    
    private static boolean writeToFile = true;

    public static boolean isWriteToFile() {
        return writeToFile;
    }

    public static void setWriteToFile(boolean _writeToFile) {
        writeToFile = _writeToFile;
    }

    public static boolean isWriteToConsole() {
        return writeToConsole;
    }

    public static void setWriteToConsole(boolean _writeToConsole) {
        writeToConsole = _writeToConsole;
    }
    
    public static void log(String msg) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date _today = Calendar.getInstance().getTime();        
        String today = df.format(_today);
        
        String logName = System.getProperty("user.dir") + "\\LMS : " + today + ".log";
        
        if (isWriteToConsole()) {
           //System.out.println(callerClassName + ": "+ msg);
           System.out.println("App Status : " + msg);
        } 
       
       if (isWriteToFile()) {
           //Must write in a log file...
           //System.out.println(callerClassName + ": "+ msg);
           //System.out.println(msg);
            try {
                saveFileRoutine(logName, msg);
            } catch (Exception ex) {
                Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
        
    }

    
    
    private static void saveFileRoutine(String fileName, String txt) throws Exception{
    try
	    {
	    FileWriter writer = new FileWriter(fileName,true);
		 
	    writer.append(txt);
            
	    writer.append('\n');
	    writer.flush();
	    writer.close();
	}
	catch(Exception e)
	{
        e.printStackTrace();
	}
  }

    /**
     * method to display the message using message dialog box
     * @param message string message to be displayed
     * @param type type indicates the icon to be shown in message
     */
    public static void showAlert(String message, Alert.AlertType type){
        Alert alert = new Alert(type);
        alert.setTitle("Library Management System");
        alert.setHeaderText("Application Diaglog: ");
        alert.setContentText(message);
        alert.showAndWait();


    }

    /**
     *
     * @param message
     */
    public static void showAlert(String message){
        showAlert(message, Alert.AlertType.INFORMATION);

    }
}
