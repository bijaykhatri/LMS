package view.controller;

import helper.Helper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.DAO.AppUserDAO;
import model.DAO.AppUserDAOImpl;
import model.POJO.AppUser;

public class BookApp extends Application {

    @Override
    public void start(Stage primaryStage) {
      //Helper.log("Working directory ="+System.getProperty("user.dir"));
        AppUser admin = new AppUser();
        admin.setUsername("admin");
        admin.setPassword("admin");
        admin.setRole(AppUser.AuthorizationLevel.ADMINISTRATOR);

        AppUserDAO adminDAO = new AppUserDAOImpl();
        adminDAO.addUser(admin);

        //for librarian
        admin.setUsername("library");
        admin.setPassword("library");
        admin.setRole(AppUser.AuthorizationLevel.LIBRARIAN);

        AppUserDAO libDAO = new AppUserDAOImpl();
        libDAO.addUser(admin);

        //for both

        admin.setUsername("root");
        admin.setPassword("root");
        admin.setRole(AppUser.AuthorizationLevel.BOTH);
        new AppUserDAOImpl().addUser(admin);
        try {
            Helper.log("Application started");
            Helper.log("Setting FXML file for Login screen");
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/FormLogin.fxml"));
            Helper.log("Loading FXML scene");
            Scene scene = new Scene(root);

            Helper.log("Setting scene stage");
            primaryStage.setScene(scene);

            primaryStage.setResizable(false);

            primaryStage.getIcons().add(new Image("/images/title-icon.jpg"));
            primaryStage.setTitle("Library Management System");

            //ToolBar myToolBar = new ToolBar();

            Helper.log("Calling Login form ");
            primaryStage.show();

        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        launch(args);
    }

}
